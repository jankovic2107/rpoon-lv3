﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.Zadatak
{
    class Logger
    {
        private static Logger instance;
        private string filePath;
        private Logger()
        {
            this.filePath = @"C:\Users\Ivan Jankovic\Desktop\lol.txt";
        }

        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }
        public void Log(string message)
        {
            using (System.IO.StreamWriter fileWriter = new System.IO.StreamWriter(this.filePath, true))
            {
                fileWriter.WriteLine(message);
            }
        }

        public string FilePath
        {
            set { this.filePath = value; }
        }
    }
}
