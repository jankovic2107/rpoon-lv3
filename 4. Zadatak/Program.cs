﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification consoleNotification = new ConsoleNotification("Ivan", "Labos", "4. zadatak", DateTime.Now, Category.ERROR, ConsoleColor.Green);
            NotificationManager notificationManager = new NotificationManager();
            notificationManager.Display(consoleNotification);
        }
    }
}
