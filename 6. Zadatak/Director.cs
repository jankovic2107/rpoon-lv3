﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.Zadatak
{
    class Director
    {
        public NotificationBuilder infoNotificatonBuilder(string Author)
        {
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            notificationBuilder.SetAuthor(Author).SetLevel(Category.INFO);
            return notificationBuilder;
        }
        public NotificationBuilder alertNotificatonBuilder(string Author)
        {
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            notificationBuilder.SetAuthor(Author).SetLevel(Category.ALERT);
            return notificationBuilder;
        }
        public NotificationBuilder errorNotificatonBuilder(string Author)
        {
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            notificationBuilder.SetAuthor(Author).SetLevel(Category.ERROR);
            return notificationBuilder;
        }
    }
}
