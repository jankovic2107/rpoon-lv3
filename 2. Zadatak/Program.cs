﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            MatrixGenerator matrixGenerator = MatrixGenerator.GetInstance();
            double[][] matrix = new double[3][];
            for(int i = 0; i < 3; i++)
            {
                matrix[i] = new double[3];
            }
            matrix = matrixGenerator.getMatrix(3, 3);
            for(int i = 0; i < 3; i++)
            {
                for(int j = 0; j < 3; j++)
                {
                    Console.Write(Math.Round(matrix[i][j], 4) + "\t");
                }
                Console.WriteLine();
            }
        }
    }
}
