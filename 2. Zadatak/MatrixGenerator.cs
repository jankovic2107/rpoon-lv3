﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.Zadatak
{
    class MatrixGenerator
    {
        private static MatrixGenerator instance;
        private RandomGenerator generator;
        private MatrixGenerator()
        {
            this.generator = RandomGenerator.GetInstance();
        }

        public static MatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new MatrixGenerator();
            }
            return instance;
        }

        public double[][] getMatrix(int x, int y)
        {
            double[][] matrix = new double[x][];
            for(int i = 0; i < x; i++)
            {
                matrix[i] = new double[y];
                for(int j =  0; j < y; j++)
                {
                    matrix[i][j] = generator.NextDouble();
                }
            }
            return matrix;
        }
    }
}
