﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationManager notificationManager = new NotificationManager();
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            notificationBuilder.SetAuthor("Marko").SetText("Danas je dan četvrtak").SetTime(DateTime.Now);
            notificationManager.Display(notificationBuilder.Build());
            
        }
    }
}
