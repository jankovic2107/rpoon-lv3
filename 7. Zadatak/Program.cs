﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification consoleNotification = new ConsoleNotification("Ivan", "Labos", "7. zadatak", DateTime.Now, Category.ERROR, ConsoleColor.Green);
            ConsoleNotification newConsoleNotification = (ConsoleNotification)consoleNotification.Clone();
            NotificationManager notificationManager = new NotificationManager();

            notificationManager.Display(consoleNotification);
            Console.WriteLine();
            notificationManager.Display(newConsoleNotification);
           

        }
    }
}
// Ima li u konkretnom slučaju razlike između plitkog i dubokog kopiranja?
// Ne, nema razlike između plitkog i dubokog kopiranja.