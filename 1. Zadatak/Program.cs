﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            string filePath = @"C:\Users\Ivan Jankovic\Desktop\lol.txt";
            Dataset dataset = new Dataset(filePath);
            Dataset newDataset = (Dataset)dataset.Clone();

            dataset.PrintData();
            Console.WriteLine();
            newDataset.PrintData();
        }
    }
}
