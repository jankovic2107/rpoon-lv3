﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1.Zadatak
{
    class Dataset : Prototype
    {
        private List<List<string>> data;

        public Dataset()
        {
            this.data = new List<List<string>>();
        }

        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }

        public void LoadDataFromCSV(string filePath)
        {
            using(System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach(string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }

        public Prototype Clone()
        {
            Dataset dataset = new Dataset();
            foreach(List<string> list in this.data)
            {
                List<string> temporaryList = new List<string>();
                foreach(string word in list)
                {
                    temporaryList.Add(word);
                }
                dataset.data.Add(temporaryList);
            }
            return dataset;
        }
        public void PrintData()
        {
            for(int i = 0; i < data.Count; i++)
            {
                for(int j = 0;j < data[i].Count; j++)
                {
                    Console.Write(data[i][j]);
                }
                Console.WriteLine();
            }
        }
    }
}
